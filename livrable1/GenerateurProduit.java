class GenerateurProduit extends Program {
    String rechercherValeur(String filename) {
        String ligne;
        int [] tab = new int [] {6, 7, 13, 7, 14};
        int i = 0;
        String nameField = "";
        String dateField = "";
        String entField = "";
        String priceField = "";
        String descriField = "";
        String resultat = "";

        extensions.File texte = new extensions.File(filename);
        while (i != 5) {
            ligne = readLine(texte);
            String res = substring(ligne, tab[i], length(ligne));
            if (i == 0) {
                nameField = res;
            }
            if (i == 1) {
                dateField = res;
            }
            if (i == 2) {
                entField = res;
            }
            if (i == 3) {
                priceField = res;
            }
            if (i == 4) {
                descriField = res;
            }
            i = i+1;
        }
        resultat = "<!DOCTYPE html>\n";
        resultat+="<html lang=\"fr\">\n";
        resultat+="  <head>\n";
        resultat+="    <title>"+nameField+"</title>\n";
        resultat+="    <meta charset=\"utf-8\">\n";
        resultat+="  </head>\n";
        resultat+="  <body>\n";
        resultat+="    <h1>"+nameField+" ("+entField+")</h1>\n";
        resultat+="    <h2>"+priceField+" (Sortie en "+dateField+")</h2>\n";
        resultat+="    <p>\n";
        resultat+=descriField+"\n";
        resultat+="    </p>\n";
        resultat+="  </body>\n";
        resultat+="</html>";
        return resultat;
    }

    void algorithm() {
        String filename = argument(0);
        println(rechercherValeur(filename));
    }
}