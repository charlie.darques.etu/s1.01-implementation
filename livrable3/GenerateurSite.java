class GenerateurSite extends Program {
    
    final char NEW_LINE = '\n';
    final String ENTETE = "<!DOCTYPE html>" + NEW_LINE +
                          "<html lang=\"fr\">" + NEW_LINE;
    final int IDX_NOM = 0;
    final int IXD_DATE = 1;
    final int IDX_ENTREPRISE = 2 ; 
    final int IDX_PRIX = 3;
    final int IDX_DESCRIPTION = 4 ; 

    String rechercherValeur(String chaine, String cle) {
        String valeur = "";
        int indice = 0;
        while (indice < length(chaine) && indice+length(cle) < length(chaine) && 
               !equals(cle, substring(chaine, indice, indice+length(cle)))) {
            indice = indice + 1;
        }
        if (indice < length(chaine)-length(cle)) {
            int indiceRetourLigne = indice;
            while (indiceRetourLigne < length(chaine) && charAt(chaine, indiceRetourLigne) != NEW_LINE) {
                indiceRetourLigne = indiceRetourLigne + 1;
            }
            valeur = substring(chaine, indice+length(cle), indiceRetourLigne);
        }
        return valeur;
    }

    String genererHead(String titre) {
        return 
            "  <head>" + NEW_LINE + 
            "    <title>" + titre + "</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">" + NEW_LINE +  
            "  </head>" + NEW_LINE;
    }

    String genererHeader(String titre) {
        return
            "    <header>" + NEW_LINE +
            "      <h1>" + titre + "</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE;
    }

    String genererNavAccueil() {
        return 
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Altair 8800</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">NeXT Computer</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Sinclair ZX Spectrum</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Amiga 1000</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">ENIAC</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
    }

    String genererNavProduits(String [][] tabProduits, int indexProduit) {
        String res = "";
        if (indexProduit == 27) {
            res = 
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + indexProduit + ".html\">"+ tabProduits[indexProduit-1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+1) + ".html\">" + tabProduits[indexProduit][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+2) + ".html\">" + tabProduits[indexProduit+1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+3) + ".html\">" + tabProduits[indexProduit+2][IDX_NOM] + "</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
        } else if (indexProduit == 28) {
            res = 
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + indexProduit + ".html\">"+ tabProduits[indexProduit-1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+1) + ".html\">" + tabProduits[indexProduit][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+2) + ".html\">" + tabProduits[indexProduit+1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
        } else if (indexProduit == 29) {
            res =
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + indexProduit + ".html\">"+ tabProduits[indexProduit-1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+1) + ".html\">" + tabProduits[indexProduit][IDX_NOM] + "</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
        } else if (indexProduit == 30) {
            res = 
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + indexProduit + ".html\">"+ tabProduits[indexProduit-1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
        } else {
            res = 
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + indexProduit + ".html\">"+ tabProduits[indexProduit-1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+1) + ".html\">" + tabProduits[indexProduit][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+2) + ".html\">" + tabProduits[indexProduit+1][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+3) + ".html\">" + tabProduits[indexProduit+2][IDX_NOM] + "</a></li>" + NEW_LINE +
            "        <li><a href=\"produit" + (indexProduit+4) + ".html\">" + tabProduits[indexProduit+3][IDX_NOM] + "</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE;
        }
        return res;
    }


    String genererContenuProduit(String nom, String date, String entreprise, String prix, String description) {
        return             
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>" + nom + " (" + entreprise + ")</h2>" + NEW_LINE +
            "        <h3>" + prix + " (Sortie en " + date + ")</h3>" + NEW_LINE +
            "        <p>" + NEW_LINE +
            description + NEW_LINE + 
            "        </p>" + NEW_LINE + 
            "      </section>" + NEW_LINE +
            "    </main>" + NEW_LINE;
    }

    String genererPageProduit(String nomFichier, String head_titre, int nb, String [][] produitsTab) {
        final String CONTENU     = fileAsString(nomFichier);
        final String NOM         = rechercherValeur(CONTENU, "nom : ");
        final String DATE        = rechercherValeur(CONTENU, "date : ");
        final String ENTREPRISE  = rechercherValeur(CONTENU, "entreprise : ");
        final String PRIX        = rechercherValeur(CONTENU, "prix : ");
        final String DESCRIPTION = rechercherValeur(CONTENU, "description : ");

        return 
            ENTETE + 
            genererHead(head_titre) + 
            "  <body>" + NEW_LINE + 
            genererHeader(head_titre) + 
            genererNavProduits(produitsTab, nb) + 
            genererContenuProduit(NOM, DATE, ENTREPRISE, PRIX, DESCRIPTION) + 
            "  </body>" + NEW_LINE + 
            "</html>" + NEW_LINE;
    }

    String genererAccueil(String head_titre) {
        return ENTETE +
            genererHead(head_titre) +
            "  <body>" + NEW_LINE + 
            genererHeader(head_titre) + 
            genererNavAccueil() +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>" + NEW_LINE +
            "          <p>" + NEW_LINE +
            "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. "+ 
            "Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique "+
            "que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui "+
            "ont contribué au développement du secteur informatique." + NEW_LINE +
            "          </p>" + NEW_LINE +
            "      </section>" + NEW_LINE +
            "    </main>" + NEW_LINE +
            "  </body>" + NEW_LINE + 
            "</html>" +NEW_LINE + NEW_LINE;
    }

    String [][] chargerProduits (String repertoire, String prefixe) {
            String [] liste = getAllFilesFromDirectory(repertoire);
            String [][] tab = new String [length(liste)][5];

            for (int file = 0; file < length(liste); file++) {
            String produit = fileAsString(repertoire + "/" + prefixe + (file + 1) + ".txt");
                tab[file][IDX_NOM] = rechercherValeur(produit, "nom : ");
                tab[file][IXD_DATE] = rechercherValeur(produit, "date : ");
                tab[file][IDX_ENTREPRISE] = rechercherValeur(produit, "entreprise : ");
                tab[file][IDX_PRIX] = rechercherValeur(produit, "prix : ");
                tab[file][IDX_DESCRIPTION] = rechercherValeur(produit, "description : ");
            }
            return tab;
        }

    String toString (String [][] tab) {
        String afficher = "";
        for (int cpt = 0; cpt < length(tab); cpt ++) {
            afficher = afficher + tab[cpt][IDX_NOM] 
                                + " (" + tab[cpt][IXD_DATE] + ") - " 
                                + tab[cpt][IDX_PRIX] + " - " 
                                + tab[cpt][IDX_DESCRIPTION] + "\n";
        }
        return afficher;
    }

    void algorithm() {
        println("Génération de la page 'index.html'");
        final String TITLE = "Ordinateurs mythiques";
        final String PAGE_ACCUEIL = genererAccueil(TITLE);
        stringAsFile("output/index.html", PAGE_ACCUEIL);

        final String prefixe = "produit";
        String [][] produitsTab = chargerProduits("data", prefixe);
        
        for (int nb = 1; nb <= length(produitsTab); nb = nb + 1) {
            final String SOURCE = "data/"   + "produit" + nb + ".txt";
            final String CIBLE  = "output/" + "produit" + nb + ".html";
            println("Génération de la page '"+ CIBLE + "' depuis le fichier source '" + SOURCE + "'");
            final String PAGE_PRODUIT = genererPageProduit(SOURCE, TITLE, nb, produitsTab);
            stringAsFile(CIBLE, PAGE_PRODUIT);
        }
        println(length(produitsTab));
        for (int i = 0; i < length(produitsTab); i ++) {
            println(produitsTab[i][0]);
        }
    }
}